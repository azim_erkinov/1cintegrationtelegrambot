from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart
from Integration.user_check import get_available_user
from loader import dp


@dp.message_handler(CommandStart())
async def bot_start(message: types.Message):
    has_access = get_available_user(message.from_user.id)['hasAccess']
    if has_access:
        await message.answer(f"Salom, {message.from_user.full_name}!")
    else:
        pass
        await message.answer("Sizga botdan foydalanish huquqi yoq")
