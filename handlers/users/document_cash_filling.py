from loader import dp
from aiogram.types import Message, CallbackQuery
from keyboards.inline.callback_data import document_cash_callback
from aiogram.dispatcher import FSMContext
from states.cash_data import CashData

"""
@dp.callback_query_handler(document_cash_callback.filter(item_name='bank-account'))
async def show_bank_account(call: CallbackQuery, state: FSMContext):
    button_text = call.data
    result = button_text.split(':')[2]
    await call.message.delete()
    await call.message.answer(f"{result=} Документ учун малумотларни киритинг")
    await CashData.details.set()
    await state.update_data(account_number=result)
    await call.answer(cache_time=60)


@dp.message_handler(state=CashData.details)
async def show_other_details(message: Message, state: FSMContext):
    details = message.text
    await state.update_data(details=details)
    data = await state.get_data()
    account_number = data["account_number"]
    details = data["details"]
    await message.answer(f"{data} {account_number} + {details}")
    await state.finish()
"""