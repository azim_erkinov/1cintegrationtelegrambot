from aiogram.types import Message, CallbackQuery, ReplyKeyboardRemove, InlineKeyboardButton
from keyboards.default.cash_menu import cash
from keyboards.default.menu_keyboard import menu
from loader import dp
from keyboards.inline.account_number_keyboard import BankAccount
from aiogram.dispatcher import FSMContext
from keyboards.inline.callback_data import document_cash_callback
from states.cash_data import CashData
from Integration.cash_integration import get_bank_accounts
from Integration.user_check import get_available_user
import requests
from requests.auth import HTTPBasicAuth
from datetime import datetime


@dp.message_handler(text='Kirim')
async def create_receipt(message: Message, state: FSMContext):
    has_access = get_available_user(message.from_user.id)['hasAccess']
    if has_access:
        bank_account = get_bank_accounts(message.from_user.id)
        account_numbers = BankAccount(bank_account).create_inline_keyboard()
        account_numbers.insert(InlineKeyboardButton(text='Ortga', callback_data='back'))
        await message.answer('Kirim: schyotni tanlang', reply_markup=ReplyKeyboardRemove())
        await message.answer('Schyotlar:', reply_markup=account_numbers)
        await CashData.operation.set()
        await state.update_data(operation='Kirim')
        await CashData.next()
    else:
        pass


@dp.callback_query_handler(text='back', state=CashData.account_number)
async def show_cash(call: CallbackQuery, state: FSMContext):
    await state.finish()
    await call.message.answer(text='Kassa operatsiyalari:', reply_markup=cash)
    await call.answer(cache_time=60)


@dp.message_handler(text='Chiqim')
async def create_receipt(message: Message, state: FSMContext):
    has_access = get_available_user(message.from_user.id)['hasAccess']
    if has_access:
        bank_account = get_bank_accounts(message.from_user.id)
        account_numbers = BankAccount(bank_account).create_inline_keyboard()
        account_numbers.insert(InlineKeyboardButton(text='Ortga', callback_data='back'))
        await message.answer('Chiqim: schyotni tanlang', reply_markup=ReplyKeyboardRemove())
        await message.answer('Schyotlar:', reply_markup=account_numbers)
        await CashData.operation.set()
        await state.update_data(operation='Chiqim')
        await CashData.next()
    else:
        pass


@dp.message_handler(text='🔙 Ortga')
async def create_receipt(message: Message):
    await message.answer('Operatsiyalar:', reply_markup=menu)


@dp.callback_query_handler(document_cash_callback.filter(item_name='bank-account'), state=CashData.account_number)
async def show_bank_account(call: CallbackQuery, state: FSMContext):
    button_text = call.data
    result = button_text.split(':')[2]
    await call.message.delete()
    await call.message.answer(f"{result} - Summa;Tavsif;Kontragent")
    await state.update_data(account_number=result)
    await call.answer(cache_time=60)
    await CashData.next()


@dp.message_handler(state=CashData.details)
async def show_other_details(message: Message, state: FSMContext):
    details = message.text
    array_details = details.split(';')
    if len(array_details) == 3:
        await state.update_data(details=details)
        data = await state.get_data()
        operation = data["operation"]
        account_number = data["account_number"]
        sum_account = array_details[0]
        payment_purpose = array_details[1] + ',' + array_details[2]
        # await message.answer(f"{operation} + {account_number} + {sum_account} + {payment_purpose}")
        url = 'http://192.168.0.254:443/shiribomusd/hs/exchange/account'
        if operation == 'Kirim':
            url = url + '/receipt'
        elif operation == 'Chiqim':
            url = url + '/debt'
        auth = HTTPBasicAuth('Venkon', '1188')
        body = dict()
        body['document_date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        body['account_number'] = account_number
        body['sum'] = float(sum_account)
        body['payment_purpose'] = payment_purpose
        req = requests.post(url, json=body, auth=auth)
        if req.json()['performed']:
            # await message.answer(str(req.status_code))
            balance = requests.get(f'http://192.168.0.254:443/shiribomusd/hs/exchange/bank-account/{account_number}',
                                   auth=auth)
            await message.answer(f"Qoldiq:{balance.json()['balance']}")
            await state.finish()
            await message.answer(text='Kassa operatsiyalari:', reply_markup=cash)
        else:
            await message.answer(f"Ma'lumotlarni qayta kiriting")
            await CashData.details.set()
    else:
        await CashData.details.set()
        await message.answer('Qayta kiriting')


@dp.message_handler(text="Qoldiq")
async def show_balance(message: Message):
    user_id = message.from_user.id
    auth = HTTPBasicAuth('Venkon', '1188')

    response_to_user = dict()
    bank_accounts = get_bank_accounts(user_id)
    for bank_account in bank_accounts:
        bank_account_str = bank_account['bankAccount']
        array_result = bank_account_str.split(',')
        account_number = array_result[0]
        balance = requests.get(f'http://192.168.0.254:443/shiribomusd/hs/exchange/bank-account/{account_number}',
                               auth=auth)
        response_to_user[bank_account_str] = balance.json()['balance']
        await message.answer(bank_account_str + ' Qoldiq: '+str(response_to_user[bank_account_str]))
