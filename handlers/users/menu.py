from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery
from keyboards.default.menu_keyboard import menu
from keyboards.default.cash_menu import cash
from loader import dp
from Integration.user_check import get_available_user


@dp.message_handler(Command('menu'))
async def show_menu(message: Message):
    has_access = get_available_user(message.from_user.id)['hasAccess']
    if has_access:
        await message.answer("Operatsiyalar:", reply_markup=menu)
    else:
        pass


@dp.message_handler(text='💰Kassa')
async def show_cash(message: Message):
    has_access = get_available_user(message.from_user.id)['hasAccess']
    if has_access:
        await message.answer(text='Kassa operatsiyalari:', reply_markup=cash)
    else:
        pass
