import requests
from requests.auth import HTTPBasicAuth


def get_available_user(user_id):
    url = f'http://192.168.0.254:443/shiribomusd/hs/exchange/users/{user_id}'
    auth = HTTPBasicAuth('Venkon', '1188')
    response = requests.get(url, auth=auth)
    available_user = response.json()
    return available_user
