import requests
from requests.auth import HTTPBasicAuth


def get_bank_accounts(user_id):
    url = f'http://192.168.0.254:443/shiribomusd/hs/exchange/users/{user_id}/bank-account'
    auth = HTTPBasicAuth('Venkon', '1188')
    response = requests.get(url, auth=auth)
    bank_account = response.json()
    return bank_account
