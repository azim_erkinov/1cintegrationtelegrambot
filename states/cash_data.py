from aiogram.dispatcher.filters.state import State, StatesGroup


class CashData(StatesGroup):
    operation = State()
    account_number = State()
    details = State()


class Access(StatesGroup):
    has_access = State()
