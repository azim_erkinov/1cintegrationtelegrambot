from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

cash = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="Kirim"),
            KeyboardButton(text="Chiqim"),
            KeyboardButton(text="Qoldiq"),
            KeyboardButton(text="🔙 Ortga")
        ]
    ],
    resize_keyboard=True
)
