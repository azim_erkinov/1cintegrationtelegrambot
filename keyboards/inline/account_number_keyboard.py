from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from keyboards.inline.callback_data import document_cash_callback


class BankAccount:
    def __init__(self, bank_accounts):
        self.bank_accounts = bank_accounts

    def create_inline_keyboard(self):
        account_numbers = InlineKeyboardMarkup(row_width=1)
        for bank_account in self.bank_accounts:
            bank_account_str = bank_account['bankAccount']
            array_result = bank_account_str.split(',')
            result = array_result[0]
            account_numbers.insert(InlineKeyboardButton(text=bank_account_str,
                                                        callback_data=document_cash_callback.new(
                                                            item_name='bank-account', account_number=result)))
        # account_numbers.insert(InlineKeyboardButton(text='Ортга', callback_data='back'))
        return account_numbers
