import cx_Freeze

executables = [cx_Freeze.Executable("app.py")]

cx_Freeze.setup(
    name="1CIntegration,
    options={"build_exe": {"packages": ["your_packages"]}},
    executables=executables
)
